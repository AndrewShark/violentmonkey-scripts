// ==UserScript==
// @name        AUR add link to show pending requests
// @namespace   Violentmonkey Scripts
// @match       https://aur.archlinux.org/pkgbase/*
// @match       https://aur.archlinux.org/packages/*
// @grant       none
// @version     1.0
// @author      Andrew Shark
// @description 02.11.2023, 02:00:00
// ==/UserScript==

// This script adds a link to show active pending requests in mailing list.
// This is only for the packages/pkgbases in which the "n pending request(s)" flag is presented in the aur action list panel.

(function () {
    let actionlist = document.getElementById("actionlist");

    if (actionlist) {
        let ul = actionlist.querySelector("ul");
        let listItems = ul.getElementsByTagName("li");

        let pendingElement = null;
        for (let i = 0; i < listItems.length; i++) {
            if (listItems[i].textContent.includes("pending")) {
                pendingElement = listItems[i];
                break;
            }
        }

        if (pendingElement) {
            let link = document.createElement("a");
            link.textContent = pendingElement.textContent;
            link.href = "https://lists.archlinux.org/archives/search?mlist=aur-requests%40lists.archlinux.org&q=" + getPackageName();

            let span = pendingElement.querySelector("span");
            span.innerHTML = "";
            span.appendChild(link);
        }
    }

    function getPackageName() {
        let currentURL = window.location.href;  // example: "https://aur.archlinux.org/packages/kde-servicemenus-rootactions?O=10";
        let packageParts = currentURL.split("/");
        return packageParts[packageParts.length - 1].split("?")[0];
    }
})();
