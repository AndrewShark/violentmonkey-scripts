#!/bin/bash

# Author: Andrew Shark
# Homepage: https://gitlab.com/AndrewShark/violentmonkey-scripts
# License: GPLv3

# This allows to edit the script in external editor (rather than in the browser).
# You need to install the package first: `pacman -S  npm`.

npx http-server -c5
# npx http-server -c1
