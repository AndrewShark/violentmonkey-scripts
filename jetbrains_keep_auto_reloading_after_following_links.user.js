// ==UserScript==
// @name        JetBrains keep auto reloading after following links
// @namespace   Violentmonkey Scripts
// @match       http://localhost:63342/*
// @grant       none
// @version     1.0
// @author      Andrew Shark
// @description 16.01.2023, 21:00:00
// ==/UserScript==

// This script allows you to follow the links in the html page served by IntelliJ Idea IDE internal web server,
// while keeping the auto update functionality working.
//
// Use case:
// This is useful when working with documentation, that renders to html. The file watcher tool (setup in IDE) detects changes
// in the documentation markup file (in my case, .docbook file), runs the convert to html tool (in my case, kde meinproc),
// then page is automatically reloaded in the browser. So you get a functionality similar to built-in instant markdown preview.
//
// Explanation of the problem:
// When you first open the html page with the "Open in | Browser" action, it has specific search part in the url that looks like this:
// "?_ijt=cs4tulta1oqb1ambsd5vkq2spi&_ij_reload=RELOAD_ON_SAVE".
// When you follow some link on the page, it follows it, but the search part if lost from url.
// So when you make additional changes to the html file itself, you should manually reload the page with F5.
//
// To keep IntelliJ Idea's web server to automatically trigger the update of the page, this script preserves this parameter.
// It inserts the search part to the href attribute of all "a" elements.


(function() {
  'use strict';
  const queryString = window.location.search;

  const links = document.querySelectorAll('a');
  links.forEach(link => {
    const originalHref = link.getAttribute('href');
    if (originalHref) {
      const url = new URL(originalHref, window.location.href);  // create a valid url object from relative link
      url.search = queryString;  // fill-in the search part
      link.setAttribute('href', url.href);  // place the changed url back to href
    }
  });

})();
