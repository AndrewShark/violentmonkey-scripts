// ==UserScript==
// @name        Changes lines links to invent.kde.org in lxr.kde.org search
// @namespace   Violentmonkey Scripts
// @match       https://lxr.kde.org/search*
// @grant       none
// @version     1.0
// @author      Andrew Shark
// @description 11.03.2024, 22:00:00
// ==/UserScript==

(function() {
    'use strict';

    let maintable = document.getElementsByClassName("searchref")[0];
    let tbody = maintable.getElementsByTagName("tbody")[0];
    let t_rows = tbody.getElementsByTagName("tr");

    function getPos(str, subStr, i) {  // finds the i'th index of subStr in str
        return str.split(subStr, i).join(subStr).length;
    }

    for (let i = 0; i < t_rows.length; i++) {
        let line_number_cell = t_rows[i].getElementsByTagName("td")[1];
        let a_el = line_number_cell.getElementsByTagName("a")[0];
        let href = a_el.href;
        let url = new URL(href);
        url.hostname = "invent.kde.org";
        url.pathname = url.pathname.replace("/source", "")
        let third_slash_index = getPos(url.pathname, "/", 3)
        url.pathname = url.pathname.substring(0, third_slash_index) + "/-/blob/master" + url.pathname.substring(third_slash_index);
        let line = url.hash.replace("#", "");
        line = Number(line);  // to remove leading zeroes
        line = "#L" + line;
        url.hash = line;
        a_el.setAttribute("href", url.href);
    }
})();
