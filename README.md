# Violentmonkey scripts

First you need to install violentmonkey extension to vivaldi.  
Then you can add scripts from this repository.  

To edit the script in external editor (for example, in webstorm) rather than in browser, use `start-http-server.sh`.  
The file name should end with `.user.js`.  
