// ==UserScript==
// @name        Gitlab Markdown Break on Enter
// @namespace   Violentmonkey Scripts
// @match       https://gitlab.com/*
// @match       https://invent.kde.org/*
// @match       https://gitlab.archlinux.org/*
// @grant       none
// @version     1.0
// @author      Andrew Shark
// @description 08.11.2023, 19:00:00
// ==/UserScript==

// This is a workaround for https://gitlab.com/gitlab-org/gitlab-foss/-/issues/57809 - Display (single) line breaks properly in markdown comments
// This script adds two spaces when you press Enter in text fields (when creating issue, commenting, adding review note).

"use strict";

const observer = new MutationObserver(function(mutationsList) {
    for (let mutation of mutationsList) {
        if (mutation.type === 'childList') {
            for (let addedNode of mutation.addedNodes) {
                if (addedNode.id === 'note_note') {
                    var elem = document.getElementById('note_note');
                    elem.addEventListener("keydown", enterise);
                }
            }
        }
    }
});

observer.observe(document.body, { childList: true, subtree: true });

function enterise(event) {
  const inputElement = document.getElementById(event.target.id)
  console.log("enterise is called with: ", inputElement)
  if (event.key === "Enter") {
    event.preventDefault();
    const selectionStart = inputElement.selectionStart;
    const selectionEnd = inputElement.selectionEnd;
    const inputValue = inputElement.value;
    const newInputValue = inputValue.slice(0, selectionStart) + "  \n" + inputValue.slice(selectionEnd);
    inputElement.value = newInputValue;
    inputElement.setSelectionRange(selectionStart + 3, selectionStart + 3);
  }
}

const elementIds = ["issue_description", "note_note", "note-body"];

elementIds.forEach((id) => {
  const element = document.getElementById(id);
  if (element) {
    element.addEventListener("keydown", enterise);
  }
});
